import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor(
    private httpClient: HttpClient
  ) { }

  ngOnInit(): void {
  }

  public create(cliente: any) {
    return this.httpClient.post(environment.restServiceUrl + 'cliente', cliente);
  }

  public findAll(){
    return this.httpClient.get(environment.restServiceUrl + 'cliente');
  }


}
