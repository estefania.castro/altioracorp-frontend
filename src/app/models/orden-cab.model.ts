import { Cliente } from "./cliente.model";
import { OrdenDet } from "./orden-det.model";

export class OrdenCab{
    public id: number;
	public fecha: String;
	public cliente: Cliente;
	public ordenDetList: OrdenDet[]
}
