import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Articulo } from 'src/app/models/articulo.model';
import { OrdenCab } from 'src/app/models/orden-cab.model';
import { OrdenService } from 'src/app/services/orden.service';

@Component({
  selector: 'app-listado-orden',
  templateUrl: './listado-orden.component.html',
  styleUrls: ['./listado-orden.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListadoOrdenComponent implements OnInit {
  @ViewChild(MatPaginator,{static : true}) paginator: MatPaginator;

  dataSource: MatTableDataSource<OrdenCab>;
  displayedColumns = ['fecha','cliente'];
  displayedArticuloList = ['nombre','codigo', 'precioUnitario'];
  expandedElement: OrdenCab | null;
  isTableExpanded = false;

  constructor(
    private ordenService: OrdenService,
  ) { }

  ngOnInit(): void {
    this.findOrden();
  }

  private findOrden() {
    this.ordenService.findAll().subscribe(
      (result: OrdenCab[]) => {
        this.dataSource = new MatTableDataSource<OrdenCab>(result);
        this.dataSource.paginator = this.paginator;
      }
    );
  }

}
