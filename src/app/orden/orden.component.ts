import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ClienteService } from '../services/cliente.service';
import { Cliente } from '../models/cliente.model';
import { Articulo } from '../models/articulo.model';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ArticuloService } from '../services/articulo.service';
import { OrdenService } from '../services/orden.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { OrdenDet } from '../models/orden-det.model';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-orden',
  templateUrl: './orden.component.html',
  styleUrls: ['./orden.component.scss']
})
export class OrdenComponent implements OnInit {
  @ViewChild(MatPaginator,{static : true}) paginator: MatPaginator;

  form: FormGroup;
  clienteList: Cliente[];
  filteredOptions: Observable<Cliente[]>;
  articuloList: Articulo[];
  articuloSeleccionadoList: OrdenDet[] = [] ;
  filteredArtOptions: Observable<Articulo[]>;
  control  = new FormControl();
  cantidad : number;

  dataSource: MatTableDataSource<OrdenDet>;
  displayedColumns = ['codigo','nombre', 'precioUnitario', 'cantidad'];

  constructor(
    private formBuilder: FormBuilder, 
    private clienteService: ClienteService,
    private articuloService: ArticuloService,
    private ordenService: OrdenService,
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(){
    this.form = this.formBuilder.group({
      id: new FormControl(''),
      fecha: new FormControl('', [Validators.required]),
      cliente: new FormControl('', [Validators.required])
    }); 
    this.control = new FormControl();
    this.articuloSeleccionadoList = [] ;
    this.getClientes();
    this.getArticulos();
  }
 
  getClientes() {
    this.clienteService.findAll().subscribe((res: any[]) => {
        this.clienteList = res;
        this.filteredOptions = this.form.get('cliente').valueChanges
        .pipe(
          startWith(''),
          map(value => typeof value === 'string' ? value : value.nombre),
          map(nombre => nombre ? this._filter(nombre) : this.clienteList.slice())
        );
    });   
  }

  displayFn(cliente: Cliente): string {
    return cliente && cliente.nombre + ' ' + cliente.apellido ? cliente.nombre + ' ' + cliente.apellido : '';
  }

  private _filter(nombre: string): Cliente[] {
    const filterValue = nombre.toLowerCase();

    return this.clienteList.filter(option => option.nombre.toLowerCase().includes(filterValue)
                                  || option.apellido.toLowerCase().includes(filterValue));
  }

  getArticulos() {
    this.articuloService.findActives().subscribe((res: any[]) => {
        this.articuloList = res;
        this.filteredArtOptions = this.control.valueChanges
        .pipe(
          startWith(''),
          map(value => typeof value === 'string' ? value : value.nombre),
          map(nombre => nombre ? this._filterArt(nombre) : this.articuloList.slice())
        );
    });   

  }

  displayFnArt(articulo: Articulo): string {
    return articulo && articulo.nombre ? articulo.nombre : '';
  }

  private _filterArt(nombre: string): Articulo[] {
    const filterValue = nombre.toLowerCase();
    return this.articuloList.filter(optionArt => optionArt.nombre.toLowerCase().includes(filterValue));
  }

  agregar(){

    if(this.control.value && this.cantidad > 0 ){
      var ar = this.control.value;
      this.articuloService.findStock(ar.id, this.cantidad).subscribe((res: any[]) => {
        var detalle = new OrdenDet(); 
        detalle.articulo = ar;
        detalle.cantidad = this.cantidad;
        detalle.precioUnitario = detalle.articulo.precioUnitario;
  
        this.articuloSeleccionadoList.push(detalle);
        this.dataSource = new MatTableDataSource<OrdenDet>(this.articuloSeleccionadoList);
        this.dataSource.paginator = this.paginator;

      });   
    }else{
      Swal.fire('', 'Debe colocar una cantidad mayor a 0', 'warning');
    }
  }

  guardar(){

    if(this.articuloSeleccionadoList.length > 0){
      const order = {
        cliente: this.form.get('cliente').value,
        fecha: this.form.get('fecha').value,
        ordenDetList: this.articuloSeleccionadoList
      };
  
      this.ordenService.create(order).subscribe(
        result => {
          this.initForm();
          Swal.fire('', 'Datos guardados correctamente', 'success');
        }
      );
    }else{
      Swal.fire('', 'Debe agregar productos a la orden', 'warning');
    }

    
  }

}
