import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(
       private snackBar: MatSnackBar) {
    }

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        //request = this.addAuthenticationToken(request);

        return next.handle(request).pipe(catchError((err: HttpErrorResponse) => {
            this.detectErrorStatus(err);
            const error = err.error.message || err.statusText;
            return throwError(error);
        }));
    }

    private detectErrorStatus(error: HttpErrorResponse): void {
        switch (error.status) {
          case 500: {
            const mensaje = error.error ? error.error.message : '';
            this.snackBar.open(mensaje, 'Error interno del servidor');
            break;
          }
          case 404:{
            const mensaje = error.error ? error.error.message : '';
            this.snackBar.open(mensaje, 'Recurso no encontrado');
            break;
          }
          case 401:
            this.snackBar.open('Usuario debe identificarse para uso de solicitud', 'No autorizado');
            break;
          case 400:
            this.snackBar.open('Existe algún dato incorrecto en la solicitud', 'Solicitud incorrecta');
            break;
          case 403:
            this.snackBar.open('El usuario no tiene suficiente permiso para esta solicitud', 'Prohibido');
            break;
          case 405:
            this.snackBar.open('Método en la solicitud no permitida', 'Método no permitido');
            break;
          case 408:
            this.snackBar.open('Tiempo para la socilitud expirada', 'Tiempo expirado');
            break;
          case 409:
              const mensaje = error.error ? error.error.message : '';
              this.snackBar.open(mensaje, 'Conflicto');
              break;
          case 429:
            this.snackBar.open('El usuario ha realizado muchas solicitudes. Inténtalo más tarde', 'Varias solicitudes');
            break;
          case 0:
            this.snackBar.open('El servicio no se encuentra disponible', 'Servicio no disponible');
            break;
          default:
            this.snackBar.open(error.message, 'Error desconocido');
        }
    }
}

export const errorInterceptorProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptor,
    multi: true
};
