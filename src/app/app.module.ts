import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule} from './material';
import { NavbarComponent } from './navbar/navbar.component';
import { ArticuloComponent } from './articulo/articulo.component';
import { ClienteComponent } from './cliente/cliente.component';
import { OrdenComponent } from './orden/orden.component';
import { HomeComponent } from './home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ArticuloDialogComponent } from './articulo/articulo-dialog/articulo-dialog.component';
import { errorInterceptorProvider } from './interceptors/error.interceptor';
import { ListadoOrdenComponent } from './orden/listado-orden/listado-orden.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ArticuloComponent,
    ClienteComponent,
    OrdenComponent,
    HomeComponent,
    ArticuloDialogComponent,
    ListadoOrdenComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    errorInterceptorProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
