import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ClienteService } from '../services/cliente.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.scss']
})
export class ClienteComponent implements OnInit {

  clienteForm: FormGroup;
  
  constructor(
    private formBuilder: FormBuilder, 
    private clienteService: ClienteService
  ) {  }

  ngOnInit() {
    this.initForm();
  }

  private initForm(){
    this.clienteForm = this.formBuilder.group({
      id: new FormControl(''),
      nombre: new FormControl('', [Validators.required]),
      apellido: new FormControl('', [Validators.required])
    }); 
  }

  public onSubmit() {
    if (this.clienteForm.valid) {
      this.clienteService.create(this.clienteForm.value).subscribe(
        result => {
          this.initForm();
          Swal.fire('', 'Datos guardados correctamente', 'success');
        }
      );
    }else{
      Swal.fire('', 'Ingresar datos requeridos', 'warning');
    }
  }

}
