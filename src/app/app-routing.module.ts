import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArticuloComponent } from './articulo/articulo.component';
import { ClienteComponent } from './cliente/cliente.component';
import { OrdenComponent } from './orden/orden.component';
import { ListadoOrdenComponent } from './orden/listado-orden/listado-orden.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'cliente',
    component: ClienteComponent
  },
  {
    path: 'articulo',
    component: ArticuloComponent
  },
  {
    path: 'orden',
    component: OrdenComponent
  },
  {
    path: 'listadoOrden',
    component: ListadoOrdenComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
