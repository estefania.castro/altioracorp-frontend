export class Articulo{
    public id: number;
    public codigo: string;
	public nombre: string;
	public precioUnitario: number;
    public stock : number;
}