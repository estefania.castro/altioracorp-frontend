import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrdenService {

  constructor(
    private httpClient: HttpClient
  ) { }

  public create(orden: any) {
    return this.httpClient.post(environment.restServiceUrl + 'orden', orden);
  }

  public findAll(){
    return this.httpClient.get(environment.restServiceUrl + 'orden');
  }
}
