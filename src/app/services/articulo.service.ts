import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArticuloService {

  constructor(
    private httpClient: HttpClient
  ) { }

  public create(cliente: any) {
    return this.httpClient.post(environment.restServiceUrl + 'articulo', cliente);
  }

  public findStock(id: any , cantidad: number){
    return this.httpClient.get(environment.restServiceUrl + 'articulo/stock/' + id + '/' + cantidad);
  }

  public findAll(){
    return this.httpClient.get(environment.restServiceUrl + 'articulo');
  }

  public update(articulo: any) {
    return this.httpClient.put(environment.restServiceUrl + 'articulo', articulo);
  } 

  public remove(id: number) {
    return this.httpClient.delete(environment.restServiceUrl + 'articulo/' + id);
  }

  public findActives(){
    return this.httpClient.get(environment.restServiceUrl + 'articulo/activos');
  }

}
