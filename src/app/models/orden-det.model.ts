import { OrdenCab } from "./orden-cab.model";
import { Articulo } from "./articulo.model";

export class OrdenDet{
    public id: number;
	public articulo: Articulo;
    public precioUnitario: number;
    public cantidad: number;
}