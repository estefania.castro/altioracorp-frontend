import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Articulo } from '../models/articulo.model';
import { ArticuloService } from '../services/articulo.service';
import { ArticuloDialogComponent } from './articulo-dialog/articulo-dialog.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-articulo',
  templateUrl: './articulo.component.html',
  styleUrls: ['./articulo.component.scss']
})
export class ArticuloComponent implements OnInit {
  @ViewChild(MatPaginator,{static : true}) paginator: MatPaginator;

  dataSource: MatTableDataSource<Articulo>;
  displayedColumns = ['codigo','nombre', 'precioUnitario', 'opciones'];

  constructor(
    private articuloService: ArticuloService,
    public dialog: MatDialog
  ) {
  }
  
  ngOnInit(): void {
    this.findArticulos();
    
  }

  private findArticulos() {
    this.articuloService.findAll().subscribe(
      (result: Articulo[]) => {
        this.dataSource = new MatTableDataSource<Articulo>(result);
        this.dataSource.paginator = this.paginator;
      }
    );
  }

  public openDialog(mode: string, articulo: Articulo) {
    const dialogRef = this.dialog.open(ArticuloDialogComponent, {
      width: '60%', height: '75%', data : {mode, articulo}
    });

    dialogRef.afterClosed().subscribe(
      result => {
        this.findArticulos();
      }
    );
  }

  public remove(id: number) {
    Swal.fire({
      title: 'Está seguro de eliminar?',
      text: 'Esta acción no se puede revertir',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, eliminar',
      cancelButtonText: 'Salir'
    }).then((result) => {
      if (result.isConfirmed) {
        this.articuloService.remove(id).subscribe(
          result => {
            Swal.fire('Eliminado!', 'Datos eliminados correctamente', 'success');
            this.findArticulos();
          }
        );
      }
    });
  }

}
