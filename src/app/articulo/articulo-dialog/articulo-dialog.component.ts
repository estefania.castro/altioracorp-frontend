import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Data } from '@angular/router';
import { ArticuloService } from 'src/app/services/articulo.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-articulo-dialog',
  templateUrl: './articulo-dialog.component.html',
  styleUrls: ['./articulo-dialog.component.scss']
})
export class ArticuloDialogComponent implements OnInit {

  articuloForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder, 
    private articuloService: ArticuloService,
    @Inject(MAT_DIALOG_DATA) public dataReceived: Data,
    public dialogRef: MatDialogRef<ArticuloDialogComponent>,
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(){
    this.articuloForm = this.formBuilder.group({
      id: new FormControl(''),
      codigo: new FormControl('', [Validators.required]),
      nombre: new FormControl('', [Validators.required]),
      precioUnitario: new FormControl('', [Validators.required]),
      stock : new FormControl('', [Validators.required]),
      activo: new FormControl(false)
    }); 

    if (this.dataReceived.mode == 'edit') {
      this.articuloForm.patchValue(this.dataReceived.articulo);
    }
  }

  public onSubmit() {
    if (this.articuloForm.value.id) {
      this.update();
    } else {
      this.save();
    }
  }

  public save(){
    if (this.articuloForm.valid) {
      this.articuloService.create(this.articuloForm.value).subscribe(
        result => {
          this.initForm();
          Swal.fire('', 'Datos guardados correctamente', 'success');
        }
      );
    }else{
      Swal.fire('', 'Ingresar datos requeridos', 'warning');
    }
  }

  public update() {
    if (this.articuloForm.valid) {
      this.articuloService.update(this.articuloForm.value).subscribe(
        result => {
          this.articuloForm.patchValue(result);
          Swal.fire('', 'Datos actualizados correctamente', 'success');
        }
      );
    }
  }

}
